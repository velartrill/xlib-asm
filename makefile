link: no.o
	ld no.o -o no -lc -lX11
conf: makefile
	head -n 9 makefile > makefile.new
	nasm -M no.asm -o no.o >> makefile.new
	echo "	nasm no.asm -felf64 -o no.o -gdwarf" >> makefile.new
	mv makefile.new makefile
clean: no *.o
	rm no *.o
no.o: no.asm imports.asm abi.asm globals.asm
	nasm no.asm -felf64 -o no.o -gdwarf
