;; functions imported from shared libraries
; libc functions
extern malloc

; xlib functions
extern XOpenDisplay
extern XDefaultScreenOfDisplay
extern XBlackPixelOfScreen
extern XWhitePixelOfScreen
extern XCreateSimpleWindow
extern XDefaultRootWindow
extern XMapWindow
extern XNextEvent
