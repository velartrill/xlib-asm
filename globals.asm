;; global variables
section .bss
	; memory management
	datsz: resq 1
	
	; xlib parameters
	xdsp: resq 1
	xscr: resq 1
	xblk: resq 1
	xwht: resq 1
	xwin: resq 1
	xev: resq 100
