bits 64
global _start ; entry point
extern _end ; size of data section at program launch

section .interp 
	; ld does not seem able to correctly infer the format of object
	; files and will link this code with the a.out interpreter,
	; producing a binary that can't be run on its own. to fix this,
	; we have to specify the ELF64 interpreter by hand.
	db "/lib64/ld-linux-x86-64.so.2", 0
	
%include "imports.asm"
%include "abi.asm"
%include "globals.asm"
	
section .text
	_start:
		; set up memory. TODO: maybe use mmap instead?
		sys.prep sys.brk, _end
		add sys.reg.2, 1024
		syscall
		
		; check if memory allocated successfully
		cmp rax, _end
			; n.b. %rax may not equal %rdi on success; kernel
			; will likely allocate more memory than requested
		mov [datsz], rax
		je fail.nomem
		
		; initialize xlib
		xor rdi, rdi ; set %rdi to NULL
		call XOpenDisplay
		mov [xdsp], rax
		
		ccall XDefaultScreenOfDisplay, rax
		mov [xscr], rax
		ccall XBlackPixelOfScreen, [xscr]
		mov [xblk], rax
		ccall XWhitePixelOfScreen, [xscr]
		mov [xwht], rax
		
		ccall XDefaultRootWindow, [xdsp]
		ccall XCreateSimpleWindow, [xdsp], rax, 0, 0, 640, \
			480, dword 0, qword [xblk], qword [xblk]
		mov [xwin], rax
		
		ccall XMapWindow, [xdsp], rax
		
	mainloop:
		mov rdi, [xdsp]
		mov rsi, xev
		call XNextEvent
			; TODO: implement event handling.
		jmp mainloop
			
		; exit program
		sys sys.exit, 0
		
	fail.nomem:
		; bitch to the user
		sys sys.write, 1, 2, fail.nomem.msg, fail.nomem.msg.len
		
		; exit program
		mov rdi, rax
		mov rax, 60
		syscall
		
		; failure message
			fail.nomem.msg: db 27, "[1merror:", 27, \
					"[0m could not allocate memory", 10
			fail.nomem.msg.len: equ $-fail.nomem.msg
